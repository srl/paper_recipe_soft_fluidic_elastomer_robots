\section{Fabrication}
\label{sec:Fabrication}
Three distinct fabrication techniques for soft actuators are presented in this section.
Table~\ref{tab:MachineTools} contains the superscript references to machine tools and materials used.

	
\subsection{Lamination Casting with Heterogenous Embeddings}
\label{subsec:Fabrication, Lamination Casting with Heterogenous Embeddings}
lamination-based casting with heterogeneous embeddings is a fabrication technique that extends current soft lithography casting processes.
%
As detailed in Section~\ref{subsec:RW Fabrication} and in Figure~\ref{fig:RW fabrication}, the outer layers of a soft robot are often cast separately using soft lithography techniques to inlay channel structures. Then, these layers are laminated together with a constraint layer to form the actuator.
%
To power actuation, supply lines are pierced through the actuator's side wall and run external to the mechanism.
%
This approach can be prohibitive in that it creates an unreliable pneumatic interface between supply lines and actuated channels and also these external supply lines can inhibit the robot's movement or otherwise obstruct it from completing its intended function.
%
By embedding heterogeneous components within the elastomer layers as they are cast, we address both of these challenges.
%
In this section, we show how the idea of soft lithography can be combined with embedding heterogenous components and that it is well-suited for realizing the ribbed body segment morphology.
%
Specifically, we illustrate this fabrication process in the context of creating both a soft ribbed manipulator and soft ribbed fish robot.

A ribbed manipulator, like that detailed in Section~\ref{subsec:Manipulators, Ribbed}, can be fabricated using lamination-based casting with heterogeneous embeddings.
%
The specific approach for fabricating a six segment manipulator is illustrated in Figure~\ref{fig:ribbed fab process}.
Here, seven constraint supports (Fig.~\ref{fig:ribbed fab process}d) are 3D printed$^1$ and placed into a constraint layer mold (Fig.~\ref{fig:ribbed fab process}f), which is also 3D printed.
The constraint film (Fig.~\ref{fig:ribbed fab process}c) is cut from a thin acetal sheet$^8$ using a laser$^2$ and inserted through the aforementioned supports.
Above and below the constraint film, eight pieces of silicone tubing (Fig.~\ref{fig:ribbed fab process}a) are threaded through the supports.
Silicone rubber$^3$ is then mixed and poured into the constraint layer mold, immersing tubing, film, and supports in a layer of elastomer to create the composite constraint layer (Fig.~\ref{fig:ribbed fab process}g).
The uncured rubber inside the mold is then immediately degassed using a vacuum chamber$^4$.
Once cured, small holes are created in the constraint layer to pierce the embedded tubing at specific locations, allowing each line to independently address a group of fluidic channels.
Elastomer pieces containing channels (Fig.~\ref{fig:ribbed fab process}b) are casted and cured separately using a similar molding technique.
Those cured elastomer pieces (Fig.~\ref{fig:ribbed fab process}b) are then carefully attached to both faces of the constraint layer using a thin layer of silicone rubber.
Lastly, the printed feet (Fig.~\ref{fig:ribbed fab process}e) are attached to the constraint supports (Fig.~\ref{fig:ribbed fab process}d) to create an attachment point for ball transfers (Fig.~\ref{fig:ribbed_manipulator_design}d).
These mechanisms help constrain the arm's motion to a plane.
\begin{figure}[htb]
\includegraphics[width=\columnwidth]{figures/fabrication/fab_ribbed_process.eps}
\caption[Fabrication process for a ribbed manipulator morphology]{Fabrication process for a ribbed manipulator: silicone tubing (a), elastomer pieces containing channels (b), constraint film (c), constraint supports (d), feet (e), constraint layer mold (f), and composite constraint layer (g).}
\label{fig:ribbed fab process}
\end{figure}

The anatomically proportioned body of a fish-like robot developed by the authors in \citet{marchese2014autonomous} and detailed in Section~\ref{subsec:Locomotion, Pneumatic Fish} was also fabricated using a similar lamination-based casting process, and this process is detailed in Figure~\ref{fig:ribbed fab process_fish}.
Supply lines that connect the posterior actuator pair are embedded within the body during step 2 (Fig.~\ref{fig:ribbed fab process_fish}-2).
\begin{figure}[htb]
\includegraphics[width=\columnwidth]{figures/fabrication/fab_ribbed_process_fish.eps}
\caption{Illustration of the soft fish body fabrication process. First, two halves of the body (\textbf{1a}), a connector piece (\textbf{1b}), and a constraining layer (\textbf{1c}) are all cast from silicone using two-part molds. Next, these four pieces are sequentially bonded together using a thin layer of silicone (\textbf{2}). Lastly, once cured the fish body is ready for operation (\textbf{3}). This figure and caption are reproduced with permission from \citet{marchese2014autonomous}.}
\label{fig:ribbed fab process_fish}
\end{figure}

\subsection{Retractable Pin Casting}
\label{subsec:Fabrication, Retractable Pin Casting}
Retractable pin casting allows the relatively simple channel structure of the cylindrical body segment to be cast without lamination.
%
This fabrication process is advantageous because it eliminates the rupture-prone seems between the channels and constraint layer seem in the ribbed morphology fabricated through lamination-based casting.
%
Additionally, retractable pin casting is well-suited for the modular fabrication of multi-body soft robots.
%
Here, segments are individually cast and then concatenated together to form the robot.
%
Specifically, in this section we demonstrate retractable pin casting in the context of fabricating a cylindrical manipular.


A cylindrical manipulator, like that detailed in Section~\ref{subsec:Manipulators, Cylindrical}, is fabricated through a retractable-pin casting using pourable silicone rubber$^{3,5}$ and 3D printed molds$^1$.
Figure~\ref{fig:cylindrical_fab} details this process.
First, each body segment is independently fabricated in steps 1-3 and later these segments are joined serially to form the arm in steps 4 and 5.
To start, a four piece mold is printed.
The mold is then poured in two steps.
In step 1, a low elastic modulus rubber$^3$ is mixed, degassed in a vacuum$^4$, and poured to form the body segment's soft outer layer shown in \emph{white}.
The mold's outer piece, one half of it is shown in \emph{green}, functions to form the segment's exterior.
Metal rods shown in \emph{pink} are inserted into the mold and are held in place by the \emph{orange} bottom piece of the mold.
These rods will form the cavities for the segment's two lateral fluidic actuation channels.
After the outer layer has cured, the \emph{red} rigid sleeve is removed in step 2 from the extruded feature of the \emph{orange} bottom piece of the mold.
This produces a cavity into which a slightly stiffer rubber$^5$ is poured, forming the segment's partially constraining inner layer shown in \emph{cyan}.
The extruded feature of the \emph{orange} bottom piece, shown by its \emph{orange} end tip, functions to produce the segment's hollow interior core.
In step 3, the body segments are removed from their molds and joined to rubber$^5$ endplates shown in \emph{cyan} using silicone adhesive$^6$.
The small \emph{yellow} channel inlets were added on one side of the \emph{pink} metal pins during step 1.
In step 4, soft silicone tubes$^7$ are joined to each embedded channel's inlet.
The resulting bundle of tubes is passed through each segment's hollow interior.
Lastly, in step 5 multiple body segments are attached at their endplates using the same adhesive$^6$.

\begin{figure}[htb]
\centering
\includegraphics[width=\columnwidth]{figures/fabrication/fab_cylindrical_process.eps}
\caption[Fabrication process for the cylindrical manipulator morphology]{Fabrication process for the cylindrical manipulator morphology: Each body segment is casted using a two step process where the outer soft layer (\textbf{1}) and inner stiffer layer (\textbf{2}) are poured. Once cured, the segments are joined to endplates using silicone adhesive (\textbf{3}). Next, silicone tubing is connected to each embedded channel and the resulting tubing bundle is run inside each segment's hollow interior (\textbf{4}). Lastly, the body segments are serially connected using adhesive to form the manipulator (\textbf{5}).}
\label{fig:cylindrical_fab}
\end{figure}

\subsection{Lost Wax Casting}
\label{subsec:Fabrication, Lost Wax Casting}
As mentioned, existing soft robots are often produced through a multi-step lamination process, which produces seams and is prone to delamination.
By abandoning the need for lamination, the retractable pin fabrication process enables seamless channel structures; however, the channel structures are limited to a relatively simple shape.
%
For these reasons, we introduce lost-wax casting as part of the fabrication process for soft actuators.
%
With this, arbitrarily shaped internal channels can be achieved to enable a wider range of applications.
%
As examples, in this section we fabricate a pleated uni-directional gripper and a ribbed soft fish tail using the lost-wax approach.

The complete fabrication process for a pleated actuator consists of eight steps that are depicted in Figure~\ref{fig:pleated_fab}.
\begin{figure*}[htb]
\centering
   \includegraphics[width=2\columnwidth]{figures/fabrication/fab_pleated_process_horizontal.eps}
      \caption[Fabrication process for the pleated actuator morphology]{Fabrication process for the pleated actuator morphology: (\textbf{A}) Pour and cure a rubber mold, (\textbf{B}) pour wax core with embedded supportive rod, (\textbf{C}) combine bottom mold, top mold and wax core using pins, (\textbf{D}) pour rubber into assembled mold, (\textbf{E}) pour stiffer rubber on top of the cured actuator to form a constraint layer, (\textbf{F}) remove cured actuator from mold, (\textbf{G}) melt out wax core from the actuator using an oven, and (\textbf{H}) add silicone tubing and plug using silicone sealant.}
      \label{fig:pleated_fab}
\end{figure*}
In step (A), harder silicone rubber$^{10}$ is poured into a mold, which contains a 3D printed model of the wax core.
In preparation for step (B), the model is removed and the rubber mold is left inside the outer mold.
Next, a rigid rod or tube, for example made of carbon fiber$^{12}$, is used as a supportive inlay for the wax core.
The rod is laid into the cavity of the rubber mold, supported on both ends by the outer mold.
This ensures that the wax core does not break when removed from the rubber mold.
Mold release spray is applied to the silicone rubber mold to ease the wax core removal process.
The wax$^{11}$ is heated up until it becomes fully liquefied.
The assembly of the rubber mold and the outer mold is heated up for a few minutes to the same temperature as the wax.
Using a syringe, the liquid wax is injected into the assembly.
Within a few minutes, the injected wax will start to solidify and significantly shrink in volume; this is counteracted by injecting more hot wax into the solidifying wax core during the cool down period.
In step (B), the wax core is first allowed to completely cool down, then it is released from the mold.
In step (C), the cooled down wax core is assembled together with the bottom mold, which defines the pleated structure of the actuator.
The mold assembly is aligned with a top mold using pins. This top mold provides additional volume to cover the wax core.
In step (D), low elastic modulus rubber$^3$ is mixed, degassed in a vacuum$^4$, and poured to form the pleats and allowed to cure.
In step (E), stiffer rubber is poured on top of the cured pleats to form a constraint layer.
In step (F), the cured actuator is removed from the mold.
In step (G), most of the wax core is melted out by placing the cured actuator into an oven in an upright position.
After this, remaining wax residues are cooked out in a boiling water bath.
Finally, in step (H) a silicone tube$^9$ and a piece of silicone cord$^13$ get covered with silicone adhesive$^6$ and are inserted into the front and back holes, respectively.
The actuator can be used as a unidirectional gripper (see Figure~\ref{fig:cylindrical_design}) or as one agonist actuated segment within a multiple body manipulator (see Section~\ref{subsec:Manipulators, Pleated}).

The actuated body of the hydraulic fish detailed in Section~\ref{subsec:Locomotion, Hydraulic Fish} is also produced via lost-wax casting. The fabrication process is depicted in Figure~\ref{fig:fabrication}.
\begin{figure}[htb]
        \centering
         \includegraphics[width=0.99\columnwidth]{figures/fabrication/fab_hydraulic_fish_tail.pdf}
         \caption[Fish tail fabrication process]{Fish tail fabrication process: (\textbf{A}) Pour and cure a rubber mold, (\textbf{B}) pour wax cores, (\textbf{C}) combine head constraint, center constraint and wax cores with tail mold halves, (\textbf{D}) pour rubber mixed with glass bubbles into assembled tail mold, (\textbf{E}) using an oven melt out wax core from the cured fish tail, and (\textbf{F}) cook out remaining wax to create desired actuator cavities.}\label{fig:fabrication}
\end{figure}
In step (A), the rubber mold is poured and cured inside an assembly consisting of an outer mold with lid and a model for the core inside of it.
In preparation for step (B), the lid and the model core are removed and the rubber mold is left inside the outer mold.
The rubber mold receives a small carbon fiber tube as an inlay in its center cavity.
This ensures that the wax core does not break when being removed from the rubber mold.
Mold release spray is applied to the silicone rubber mold to ease the wax core removal process.
The wax is heated up until it becomes fully liquefied.
The assembly of rubber mold and outer mold is heated up for a few minutes to the same temperature as the wax.
Using a syringe, the liquid wax is injected into the assembly.
Within a few minutes, the injected wax will start to solidify and significantly shrink in volume; this is counteracted by injecting more hot wax into the solidifying wax core during the cool down.
In step (B), the wax core is first allowed to completely cool down, then it is released from the mold.
In step (C), a head constraint, a center constraint, and two wax cores are assembled together inside the tail mold halves using spacers, positioning pins and screws.
In step (D), a mix of silicone rubber with glass bubbles is poured into the tail assembly and allowed to cure.
In step (E), most of the wax core is melted out by placing the fish tail in an upright position into an oven. Finally, in step (F) the remaining wax residues are cooked out in a boiling water bath.

\begin{table}[htb]
\caption{Commercially Available Tools and Equipment}
\centering
\begin{tabular}{c l l}
\hline
\hline
\# & Product Name & Company\\
\hline
1 & Fortus 400mc & Stratasys\\
2 & VLS3.50 & Universal Laser Systems\\
3 & Ecoflex 0030 & Smooth-On\\
4 & AL Cube & Abbess Instr. \& Systems\\
5 & Mold Star 15 & Smooth-On\\
6 & Silicone Sealant 732 & Dow Corning\\
7 & PN 51845K52 & McMaster\\
8 & PN 5742T51 & McMaster\\
9 & PN 51845K53 & McMaster\\
10 & Mold Star 30 & Smooth-On\\
11 & Beeswax & Jacquard\\
12 & PN 2153T31& McMaster\\
13 & PN 9808K21& McMaster\\
\hline
\end{tabular}
\label{tab:MachineTools}
\end{table} 